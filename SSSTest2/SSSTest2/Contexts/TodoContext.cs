﻿using Microsoft.EntityFrameworkCore;
using SSSTest2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSSTest2.Contexts
{
    public class TodoContext : DbContext
    {
		public TodoContext(DbContextOptions<TodoContext> options) : base(options)
		{

		}

		public DbSet<Todo> Todos { get; set; }
    }
}
