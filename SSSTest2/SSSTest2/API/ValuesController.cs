﻿using Microsoft.AspNetCore.Mvc;
using SSSTest2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSSTest2.API
{
	[Route("api/[controller]")]
	public class ValuesController : ControllerBase
	{
		string savedValue;

		// GET api/values
		[HttpGet]
		public IEnumerable<string> Get()
		{
			return new string[] { "value3", "value4" };
		}

		// GET api/values/5
		[HttpGet("{id}")]
		public IActionResult Get(int id)
		{
			if (id <= 0)
				return NotFound("-_-");
			else if (id == 1)
				return Ok("Blarg!");
			else
			{
				string returnBlarg = "Blargity";

				for (int i = 0; i < id - 1; i++)
					returnBlarg += " blarg";

				returnBlarg += "!";
				return Ok(returnBlarg);
			}
		}

		// POST api/values
		[HttpPost]
		public string Post([FromBody]MyPostModel postModel)
		{
			savedValue = postModel.userSocs[0];
			return savedValue;
		}

		// PUT api/values/5
		[HttpPut("{id}")]
		public string Put(int id, [FromBody]MyPostModel postModel)
		{
			savedValue = postModel.userSocs[0];
			return savedValue;
		}

		// DELETE api/values/5
		[HttpDelete("{id}")]
		public void Delete(int id)
		{
		}
	}
}